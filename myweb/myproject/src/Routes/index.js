import React, { Component } from 'react';
import {BrowserRouter , Route, Switch} from 'react-router-dom';
import Login from "../components/Login";

class index extends Component {
    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <Route exact path="/login" component={Login}/>
                </Switch>
            </BrowserRouter>
            
        );
    }
}

export default index;